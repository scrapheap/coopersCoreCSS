# Cooper's Core CSS

## Description
Cooper's Core CSS is a simple CSS style sheet that you can use when starting to develop a new site.
Where possible Cooper's Core CSS styles based upon the HTML content without the requirement to add lots
of additional styling information to the HTML itself. This helps keep the HTML clean and easier to read
and maintain, it also promotes consistency of styling between different instances of the same element.

To see what the base style looks like check out the [Demo](https://www.heckrothindustries.co.uk/demos/coopersCoreCSS/).

## Usage
Simply copy the coopersCoreCSS.min.css file into the same directory as your HTML pages and add
the following lines at the start of your page's header:

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" media="all" href="coopersCoreCSS.min.css">
```

Most of the styles will take effect automatically but the following you need to choose to use.

### Counters (sections/headings, tables, figures)
There are a number of counters available for use that can be displayed at the start of headings, tables
and figures. Active counter types are controlled by adding a `data-core-counters` attribute to the `body`
tag. Current valid counters that you can specify are

* figures
* tables
* sections

The sections counter can also be fine tuned by specifying a `data-core-section-counter-depth` attribute
(valid values are 1 to 6, with the default of 1). If you specify a depth of 1 then only sections will
be counted and h2 to h6 headings will not be preceded by their counters, specify a depth of 3 and h1,
h2 and h3 headings will be preceded by their counters, but h4, h5 and h6 will not. Specify a depth of 6
and all headings will be preceded by their counters.

Example of the data-core-counters and data-core-section-counter-depth attributes in use:
```html
<body data-core-counters="figures tables sections" date-core-section-counter-depth="6">
```

### Keeping floats with headers
To keep floats within a set depth of headers then set the `keep-floats-within-heading-depth` attribute to
the depth level of the headers you wish to keep floats within. E.g.

```html
<body keep-floats-within-heading-depth="3">
```

### Asides
Asides will take up 30% of their container and if you specify a class of `core-left` or `core-right` for it then
it will be floated to the relevant side, but on small screens will become a standard block element to
keep it readable.

### Tables
The table styling works best if you put all heading rows for a table in their own `thead` group, all
footer rows in their own `tfoot` group and all other rows in the `tbody` group. Putting everything in
the `tbody` group will work but won't look as good and your HTML will be less descriptive.

### Forms
Form layouts are usually highly dependant on the forms themselves, but their is a common simple form
layout available. To use it add the class `core-simpleForm` to the form's container, place each input
field and label in it's own `div` with the input field appearing first and the label second. E.g.

```html
<fieldset class="core-simpleForm">
    <legend>Fields</legend>
    <div>
        <input type="text" id="name" required>
        <label for="name">name</label>
    </div>
    <div>
        <input type="checkbox" id="checkbox">
        <label for="checkbox">checkbox</label>
    </div>
    <div>
        <select id="select" required>
            <option>First</option>
            <option>Second</option>
            <option>Third</option>
        </select>
        <label for="select">select</label>
    </div>
    <div>
        <textarea id="textarea">Lots of lovely text
with multiple lines</textarea>
        <label for="textarea">textarea</label>
    </div>
</fieldset>
```

By default the labels will be given a width of 20% of their container and the label for any inputs marked
as required will be proceeded by `\*` to show that they're required fields.

### Code
By default `code` tags are inline element's and are styled as such, but if you wrap your `code` tags in
`pre` tags then they will be styled as a block of code.

### Links
Links that have a target of \_blank have an icon appended to them to show that they'll open in a new window or tab.

If a link is a download then giving it a class of `core-download` will append an icon it indicating it is a download.

### Navigation Menus
There are two options for menus Navigation Bars and Navigation Columns. Both of them require you to put your menu
entries in a `nav` container as a series of `a` tag links. To mark an `a` tag as being the current active menu option
give it a `data-core-active` attribute with a value of true. E.g.

```html
<nav>
    <a href="#menu" data-core-active="true">Menu</a>
    <a href="#profile">Profile</a>
    <a href="#signOut">Sign Out</a>
</nav>
```

#### Navigation Bar
To use a horizontal menu simply give the `nav` tag a class of `core-navBar`. E.g.

```html
<nav class="core-navBar">
    <a href="#menu" data-core-active="true">Menu</a>
    <a href="#profile">Profile</a>
    <a href="#signOut">Sign Out</a>
</nav>
```

#### Navigation Column
To use a vertical menu column on the left hand side of the screen then you need to wrap the `nav` element and
the page content's container element in a `div` with a class of `core-navCol`. To place the menu column on the
right hand side then simple add a class of `core-right` to the `nav` tag.

```html
<div class="core-navCol">
    <nav>
        <a href="#menu">Menu</a>
        <a href="#profile" data-core-active="true">Profile</a>
        <a href="#signOut">Sign Out</a>
    </nav>
    <div>
        <p>Page content goes here</p>
    </div>
</div>
```

If the size of the window is less than the small screen size (640px by default) then `navCol` will revert to a
`navBar` which will avoid wasting space on the navigation column.

## Tags
If an element is a tag then giving it a class of `core-tag` will render it as a tag. E.g.

```html
<span class="core-tag">Perl</span> <span class="core-tag">Programming Languages</span>
```

## Styles
Some additional formatting styles can be activated by adding them to the data-core-styles attribute on a
container element, to apply the style to the whole document then add them to the `body` tag. E.g.

```html
<body data-core-styles="3D">
```

Current styles available are:

3D
: Simple box-shadows are added to certain elements to give a more 3 dimensional feel to the page.

curves
: border-radius is applied to certain elements to curve their corners.

## Customisation
If you're familiar with [Sass](http://sass-lang.com/) then you can build your own custom variation.
If you just want to change colours then check out the scss/\_overrides.scss file, which can be used to
override the default settings.

## License
Cooper's Core CSS is made available under a MIT License, please see the License file for full details.
