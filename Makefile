

.PHONY: all
all: html/coopersCoreCSS.css html/coopersCoreCSS.min.css

html/coopersCoreCSS.css: scss/*.scss
	sass scss/coopersCoreCSS.scss > $@

html/coopersCoreCSS.min.css: scss/*.scss
	sass --style=compressed scss/coopersCoreCSS.scss > $@
